# Getting started

``` bash
# Start containers:
make dev

# Load fixtures
docker-compose exec php bin/console hautelook:fixtures:load

# Create JWT keys
docker-compose exec php bin/console lexik:jwt:generate-keypair --skip-if-exists
```

