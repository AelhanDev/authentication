<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\PasswordChange;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class PasswordChangeInputDataTransformer
 */
class PasswordChangeInputDataTransformer implements DataTransformerInterface
{
    public function __construct(private ValidatorInterface $validator, private UserPasswordHasherInterface $passwordHasher)
    {
    }

    /**
     * Perform DTO transformation
     *
     * @param PasswordChange $object  The DTO hydrated with request body
     * @param string         $to      The class name to transform $object into
     * @param array          $context Contains the resource to populate
     *
     * @return UserInterface
     */
    public function transform($object, string $to, array $context = []): UserInterface
    {
        $this->validator->validate($object);

        /** @var UserInterface $user */
        $user = $context['object_to_populate'];

        return $user->setPassword(
            $this->passwordHasher->hashPassword($user, $object->newPassword)
        );
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof User || empty($context['input']['class'])) {
            return false;
        }

        return User::class === $to && PasswordChange::class === $context['input']['class'];
    }
}
