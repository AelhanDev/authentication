<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Avatar;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

/**
 * In charge of injecting current user in an Avatar being created
 */
class InjectUserSubscriber implements EventSubscriberInterface
{
    public function __construct(private Security $security)
    {
    }

    public function injectUser(RequestEvent $event): void
    {
        $avatar = $event->getRequest()->attributes->get('data');
        $method = $event->getRequest()->getMethod();

        if ($this->supports($avatar, $method)) {
            $avatar->setUser($this->security->getUser());
        }
    }


    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['injectUser', EventPriorities::POST_DESERIALIZE],
        ];
    }

    /**
     * Return whether this subscriber applies for current request
     *
     * @param mixed  $subject Subject of the request
     * @param string $method  Method of the request
     *
     * @return bool
     */
    public function supports(mixed $subject, string $method): bool
    {
        return $subject instanceof Avatar && $method === Request::METHOD_POST;
    }
}
