<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Security\User\UserPasswordGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * Class UserSubscriber
 */
class UserPasswordSubscriber implements EventSubscriberInterface
{
    /**
     * @param UserPasswordGeneratorInterface $generator
     */
    public function __construct(private UserPasswordGeneratorInterface $generator)
    {
    }

    /**
     * As long as we don't allow to manually set the user password when creating, generate a random password for a user
     * being created
     *
     * @param ViewEvent $event
     */
    public function generatePasswordForNewUser(ViewEvent $event)
    {
        $user   = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($this->isNewUser($user, $method)) {
            $this->generator->generateRandomPassword($user);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                'generatePasswordForNewUser', EventPriorities::PRE_VALIDATE,
            ],
        ];
    }

    /**
     * Return whether user is being created
     *
     * @param mixed  $user
     * @param string $method
     *
     * @return bool
     */
    public function isNewUser(mixed $user, string $method): bool
    {
        return $user instanceof PasswordAuthenticatedUserInterface && Request::METHOD_POST === $method && null === $user->getPassword();
    }
}
