<?php

declare(strict_types=1);

namespace App\Security\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * This user provider should only be used to fetch users by GUID.
 */
class EntityUserGuidProvider implements UserProviderInterface
{
    public function __construct(private EntityManagerInterface $manager)
    {
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        throw new \Exception('Should never refresh user');
    }

    /**
     * @inheritDoc
     */
    public function supportsClass(string $class): bool
    {
        return $class === User::class;
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function loadUserByUsername(string $username): ?User
    {
        return $this->loadUserByIdentifier($username);
    }

    /**
     * @inheritDoc
     */
    public function loadUserByIdentifier(string $identifier): ?User
    {
        return $this->manager->getRepository(User::class)->loadByGuid($identifier);
    }
}
