<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * In charge of checking user account status before authentication
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * Prevent authentication of locked accounts
     *
     * @inheritDoc
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if ($user instanceof User && $user->getLockDate()) {
            throw new LockedException();
        }
    }

    /**
     * @inheritDoc
     */
    public function checkPostAuth(UserInterface $user): void
    {
        // Nothing special here
    }
}
