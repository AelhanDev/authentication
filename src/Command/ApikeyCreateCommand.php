<?php

declare(strict_types=1);

namespace App\Command;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\ApiKey;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:apikey:create',
    description: 'Create an API key',
)]
/**
 * Class ApikeyCreateCommand
 */
class ApikeyCreateCommand extends Command
{
    /**
     * @param EntityManagerInterface $manager
     * @param ValidatorInterface     $validator
     * @param string|null            $name
     */
    public function __construct(private EntityManagerInterface $manager, private ValidatorInterface $validator, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('usage', InputArgument::REQUIRED, 'Usage of the API key')
            ->addOption('token', 't', InputOption::VALUE_REQUIRED, 'password (if none, one will be randomly generated)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $usage = $input->getArgument('usage');
            $token = $input->getOption('token');
            $apiKey = $this->manager->getRepository(ApiKey::class)->createApiKey($usage, $token);
            $token = $apiKey->getPlainToken();
            $apiKey->eraseCredentials();

            $this->validator->validate($apiKey);
            $this->manager->persist($apiKey);
            $this->manager->flush();
        } catch (\Throwable $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }
        $id = $apiKey->getId();
        $io->success('ApiKey created successfully');
        $io->info("id: $id");
        $io->info($token);

        return Command::SUCCESS;
    }
}
