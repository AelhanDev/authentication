<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ConfirmEmailTokenRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Token for email change confirmation
 */
#[ORM\Entity(repositoryClass: ConfirmEmailTokenRepository::class)]
#[UniqueEntity(['email'])]
class ConfirmEmailToken
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private User $user;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank]
    private string $hashedToken;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $requestedAt;

    /**
     * The email to be confirmed
     */
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    private string $email;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Assert\Expression('value > this.getRequestedAt()')]
    private \DateTimeImmutable $expiresAt;

    /**
     * @param User               $user
     * @param \DateTimeImmutable $expiresAt
     * @param string             $email
     * @param string             $hashedToken
     */
    public function __construct(User $user, \DateTimeImmutable $expiresAt, string $email, string $hashedToken)
    {
        $this->user        = $user;
        $this->requestedAt = new \DateTimeImmutable('now');
        $this->expiresAt   = $expiresAt;
        $this->email       = $email;
        $this->hashedToken = $hashedToken;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getHashedToken(): string
    {
        return $this->hashedToken;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getRequestedAt(): \DateTimeImmutable
    {
        return $this->requestedAt;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->expiresAt->getTimestamp() <= time();
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getExpiresAt(): \DateTimeImmutable
    {
        return $this->expiresAt;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
