<?php

declare(strict_types=1);

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\ApiKey;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Attribute\AsController;

/**
 * Controller for creating an ApiKey resource
 */
#[AsController]
class CreateApiKey
{
    public function __construct(private EntityManagerInterface $manager, private ValidatorInterface $validator)
    {
    }

    public function __invoke(ApiKey $data): ApiKey
    {
        $newApiKey = $this->manager
            ->getRepository(ApiKey::class)
            ->createApiKey($data->getUsage());

        $this->validator->validate($newApiKey);

        $this->manager->persist($newApiKey);
        $this->manager->flush();

        return $newApiKey;
    }
}
