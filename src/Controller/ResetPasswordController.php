<?php

declare(strict_types=1);

namespace App\Controller;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

/**
 * Class ResetPasswordController
 */
#[Route('reset_password')]
class ResetPasswordController
{
    public function __construct(private ValidatorInterface $validator, private ResetPasswordHelperInterface $resetPasswordHelper, private EntityManagerInterface $entityManager, private TranslatorInterface $translator)
    {
    }

    #[Route(path: '', methods: ['POST'])]
    public function request(MailerInterface $mailer, Request $request): JsonResponse
    {
        $email      = $request->toArray()['email'] ?? null;
        $violations = $this->validator->validate($email, [
            new NotBlank(),
            new Email(),
        ]);

        if ($violations->count()) {
            throw new ValidationException($violations);
        }

        return $this->processSendingPasswordResetEmail($email, $mailer);
    }

    #[Route(path: '/{token}', requirements: ['token' => '[a-zA-Z0-9]+'], methods: ['POST'])]
    public function reset(UserPasswordHasherInterface $passwordEncoder, Request $request, string $token): JsonResponse
    {
        $password   = $request->toArray()['password'] ?? null;
        $violations = $this->validator->validate($password, [
            new NotBlank(),
            new Length(min: 6),
        ]);

        if ($violations->count()) {
            throw new ValidationException($violations);
        }

        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $e) {
            throw new BadRequestException(previous: $e);
        }

        // A password reset token should be used only once, remove it.
        $this->resetPasswordHelper->removeResetRequest($token);

        // Encode the plain password, and set it.
        $this->entityManager
            ->getRepository(User::class)
            ->upgradePassword($user, $passwordEncoder->hashPassword($user, $password));

        return new JsonResponse();
    }

    /**
     * Process sending password reset email
     *
     * @param mixed           $email
     * @param MailerInterface $mailer
     *
     * @return JsonResponse
     */
    protected function processSendingPasswordResetEmail(string $email, MailerInterface $mailer): JsonResponse
    {
        $response = new JsonResponse(status: 202);
        $user     = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            return $response;
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface) {
            return $response;
        }

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('password_recovery.email.title'))
            ->htmlTemplate('reset_password/email.html.twig')
            ->context([
                'resetToken' => $resetToken,
            ]);

        $mailer->send($email);

        return $response;
    }
}
