<?php

declare(strict_types=1);

namespace App\Controller;

use App\ConfirmEmail\ConfirmEmailHelper;
use App\ConfirmEmail\ConfirmEmailTokenException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConfirmEmailController
 */
class ConfirmEmailController
{
    /**
     * @param ConfirmEmailHelper $helper
     */
    public function __construct(private ConfirmEmailHelper $helper)
    {
    }

    #[Route('/confirm_email', name: 'app_confirm_email', methods: ['POST'])]
    /**
     * Do confirm the new email account of a user
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function confirm(Request $request): JsonResponse
    {
        $token = $request->toArray()['token'] ?? null;

        try {
            $this->helper->validateTokenAndUpdateUser($token);
        } catch (ConfirmEmailTokenException $exception) {
            throw new BadRequestException($exception->getMessage(), previous: $exception);
        }

        return new JsonResponse();
    }
}
