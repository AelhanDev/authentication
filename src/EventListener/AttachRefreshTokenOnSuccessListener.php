<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\ApiKey;
use Gesdinet\JWTRefreshTokenBundle\EventListener\AttachRefreshTokenOnSuccessListener as Decorated;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

/**
 * Decorates Gesdinet's AttachRefreshTokenOnSuccessListener in order to disable refresh_token generation for ApiKey
 */
class AttachRefreshTokenOnSuccessListener
{
    public function __construct(private Decorated $decorated)
    {
    }

    public function attachRefreshToken(AuthenticationSuccessEvent $event)
    {
        // Do not generate refresh token for ApiKeys
        if (!$event->getUser() instanceof ApiKey) {
            $this->decorated->attachRefreshToken($event);
        }
    }
}
