<?php

namespace App\Dto;

use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Data transfert object representing a password change
 */
class PasswordChange
{
    /**
     * The current password of the user
     */
    #[UserPassword]
    #[Groups('user:input')]
    public string $currentPassword = '';

    /**
     * The new password of the user
     */
    #[NotBlank]
    #[Length(min: 6)]
    #[Groups('user:input')]
    public string $newPassword = '';
}
