<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpClient\Exception\ClientException;

/**
 * Class ChangeUserPasswordTest
 *
 * @group functional
 */
class ChangeUserPasswordTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Assert password change fail using invalid data.
     */
    public function testChangePasswordFail(): void
    {
        $client = $this->createAuthenticatedClient();
       /** @var User $user */
        $user = static::getContainer()->get(UserRepository::class)->findOneByEmail('bob@example.com');

        $response = $client->request('POST', sprintf('/users/%s/change_password', $user->getId()), [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json'    => [
                'currentPassword' => '',
                'newPassword'     => '',
            ],
        ]);

        $this->assertResponseStatusCodeSame(422);

        $json = $response->getContent(false);

        $this->assertStringContainsString('This value should not be blank', $json);
        $this->assertStringContainsString('This value should be the user\u0027s current password', $json);
    }

    /**
     * Assert a user can't change someone else password
     */
    public function testCantChangeSomeoneElsePassword(): void
    {
        $client = $this->createAuthenticatedClient();
        /** @var User $user */
        $user = static::getContainer()->get(UserRepository::class)->findOneByEmail('alice@example.com');

        $client->request('POST', sprintf('/users/%s/change_password', $user->getId()), [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json'    => [
                'currentPassword' => 'foo',
                'newPassword'     => 'bar',
            ],
        ]);

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * Assert password change succeed.
     */
    public function testChangePasswordTooShort(): void
    {
        $client = $this->createAuthenticatedClient();
        /** @var User $user */
        $user = static::getContainer()->get(UserRepository::class)->findOneByEmail('bob@example.com');

        $client->request('POST', sprintf('/users/%s/change_password', $user->getId()), [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'currentPassword' => 'foo',
                'newPassword'     => 'bar',
            ],
        ]);

        $this->assertResponseStatusCodeSame(422);
    }

    /**
     * Assert password change succeed.
     */
    public function testChangePassword(): void
    {
        $client = $this->createAuthenticatedClient();
        /** @var User $user */
        $user = static::getContainer()->get(UserRepository::class)->findOneByEmail('bob@example.com');

        $client->request('POST', sprintf('/users/%s/change_password', $user->getId()), [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'currentPassword' => 'foo',
                'newPassword'     => '123456',
            ],
        ]);

        $this->assertResponseStatusCodeSame(204);

        // As credentials changed, expect authentication to fail
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage('HTTP 401 returned for "http://example.com/authentication_token".');

        $this->createAuthenticatedClient();
    }
}
