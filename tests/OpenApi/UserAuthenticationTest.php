<?php

declare(strict_types=1);

namespace App\Tests\OpenApi;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

/**
 * Class UserAuthenticationTest
 *
 * @group functional
 */
class UserAuthenticationTest extends ApiTestCase
{
    use ReloadDatabaseTrait;

    /**
     * Test JWT authentication
     */
    public function testLogin(): void
    {
        $client = self::createClient();

        // Retrieve a token
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => 'bob@example.com',
                'password' => 'foo',
            ],
        ]);

        $json = $response->toArray();
        $this->assertResponseIsSuccessful();

        $this->assertArrayHasKey('token', $json);
        $this->assertArrayHasKey('refresh_token', $json);

        // test not authorized
        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(401);

        // test authorized
        $client->request('GET', '/', ['auth_bearer' => $json['token']]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Assert login fails using invalid credentials.
     */
    public function testLoginFail(): void
    {
        $client   = static::createClient();
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => 'bob@example.com',
                'password' => 'wrong password',
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
        $this->assertStringContainsString('Invalid credentials', $response->getContent(false));
    }

    /**
     * Test refresh-token endpoint expecting success
     */
    public function testRefreshJwt(): void
    {
        $client = self::createClient();

        // Retrieve a token
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => 'bob@example.com',
                'password' => 'foo',
            ],
        ]);

        $json = $response->toArray();

        // Then, try to refresh JWT
        $client->request('POST', '/refresh_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'refresh_token' => $json['refresh_token'],
            ],
        ]);

        $this->assertResponseIsSuccessful();
    }
}
