<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Organisation;
use App\Entity\UserOrganisation;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Test class of entity Organisation
 *
 * @group functional
 */
class OrganisationTest extends KernelTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Test deletion of an organisation having users expecting related UserOrganisation to be cascaded removed
     */
    public function testDeleteOrganisationHavingUsers(): void
    {
        /** @var EntityManagerInterface $manager */
        $manager      = static::getContainer()->get(EntityManagerInterface::class);
        $organisation = $manager->getRepository(Organisation::class)->findOneBy([]);

        // Save ID before removing entity
        $id = $organisation->getId();

        $manager->remove($organisation);
        $manager->flush();

        // Expect all related UserOrganisations to be cascaded removed
        $actual = $manager->getRepository(UserOrganisation::class)->findBy(['organisation' => $id]);
        $this->assertEmpty($actual);
    }

    /**
     * Test validation with invalid data expecting violations
     */
    public function testValidationFail(): void
    {
        $organisation = new Organisation();
        $organisation->setName(str_repeat('a', 256));

        $violations = $this->getValidator()->validate($organisation);

        $this->assertGreaterThan(0, $violations->count());
        $this->assertEquals('This value is too long. It should have 255 characters or less.', $violations->get(0)->getMessage());
    }

    /**
     * Test validation expecting success
     */
    public function testValidation(): void
    {
        $organisation = new Organisation();
        $organisation->setName('Anything');

        $violations = $this->getValidator()->validate($organisation);

        $this->assertCount(0, $violations);
    }

    private function getValidator(): ValidatorInterface
    {
        return Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
    }
}
