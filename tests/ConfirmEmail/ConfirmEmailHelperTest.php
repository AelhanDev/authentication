<?php

declare(strict_types=1);

namespace App\Tests\ConfirmEmail;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\ConfirmEmail\ConfirmEmailHelper;
use App\ConfirmEmail\ExpiredConfirmEmailTokenException;
use App\ConfirmEmail\InvalidConfirmEmailTokenException;
use App\Entity\ConfirmEmailToken;
use App\Entity\User;
use App\Repository\ConfirmEmailTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfirmEmailHelper
 */
class ConfirmEmailHelperTest extends TestCase
{
    private ConfirmEmailTokenRepository $repository;
    private EntityManagerInterface $manager;
    private ValidatorInterface $validator;
    private ConfirmEmailHelper $helper;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->repository = $this->createMock(ConfirmEmailTokenRepository::class);
        $this->manager    = $this->createMock(EntityManagerInterface::class);
        $this->validator  = $this->createMock(ValidatorInterface::class);
        $this->helper     = new ConfirmEmailHelper($this->manager, $this->validator);

        $this->manager
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($this->repository);
    }

    /**
     * Test token generation expecting success
     */
    public function testGenerateConfirmToken(): void
    {
        $user     = new User();
        $token    = new ConfirmEmailToken($user, new \DateTimeImmutable('+10 seconds'), 'bob@example.com', 'hashed_value');
        $newEmail = 'bob@example.com';

        $this->repository
            ->expects($this->once())
            ->method('createResetPasswordRequest')
            ->willReturn($token);

        // Expect manager to persist & flush generated token
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($token);
        $this->manager
            ->expects($this->once())
            ->method('flush');

        $actual = $this->helper->generateConfirmToken($user, $newEmail);

        $this->assertInstanceOf(ConfirmEmailToken::class, $actual);
    }

    /**
     * Test validation of an unknown token result in failure
     */
    public function testValidateInvalidToken(): void
    {
        $this->expectException(InvalidConfirmEmailTokenException::class);

        $this->repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['hashedToken' => 'foo_token'])
            ->willReturn(null);

        $this->helper->validateTokenAndUpdateUser('foo_token');
    }

    /**
     * Test validation of an expired token result in failure
     */
    public function testValidateExpiredToken(): void
    {
        $this->expectException(ExpiredConfirmEmailTokenException::class);

        $user = new User();
        // Expect token to be expired for 10 seconds
        $token = new ConfirmEmailToken($user, new \DateTimeImmutable('-10 seconds'), 'bob@example.com', 'hashed_value');

        $this->repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['hashedToken' => 'foo_token'])
            ->willReturn($token);

        $this->helper->validateTokenAndUpdateUser('foo_token');
    }

    /**
     * Test token validation succeed
     */
    public function testValidateToken(): void
    {
        $user  = (new User())->setEmail('john@example.comm');
        $token = new ConfirmEmailToken($user, new \DateTimeImmutable('+10 seconds'), 'bob@example.com', 'hashed_value');

        // Expect user to be persisted
        $this->manager
            ->expects($this->once())
            ->method('persist')
            ->with($user);

        // Expect token to be removed
        $this->manager
            ->expects($this->once())
            ->method('remove')
            ->with($token);

        $this->manager
            ->expects($this->once())
            ->method('flush');
        $this->manager
            ->expects($this->once())
            ->method('commit');

        $this->repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['hashedToken' => 'foo_token'])
            ->willReturn($token);

        // Expect transaction to be committed
        $this->manager
            ->expects($this->once())
            ->method('commit');

        $actual = $this->helper->validateTokenAndUpdateUser('foo_token');

        $this->assertSame($user, $actual);
        $this->assertEquals('bob@example.com', $user->getEmail());
    }

    /**
     * Test token validation when error occurred expecting transaction to be rolled back.
     */
    public function testValidateRollbackOnException(): void
    {
        $expectedException = new \Exception('an error occurred');

        // Expect caught exception to be released
        $this->expectException($expectedException::class);
        $this->expectExceptionMessage($expectedException->getMessage());

        $user  = (new User())->setEmail('john@example.com');
        $token = new ConfirmEmailToken($user, new \DateTimeImmutable('+10 seconds'), 'bob@example.com', 'hashed_value');

        // Expect exception on persistance
        $this->manager
            ->method('persist')
            ->willThrowException($expectedException);

        // Expect manager to rollback on exception
        $this->manager
            ->expects($this->once())
            ->method('rollback');

        // Expect a matching token to be found
        $this->repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['hashedToken' => 'foo_token'])
            ->willReturn($token);

        $this->helper->validateTokenAndUpdateUser('foo_token');
    }
}
