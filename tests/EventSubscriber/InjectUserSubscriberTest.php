<?php

declare(strict_types=1);

namespace App\Tests\EventSubscriber;

use App\Entity\Avatar;
use App\Entity\User;
use App\EventSubscriber\InjectUserSubscriber;
use App\Kernel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Security;

/**
 * Test class of InjectUserSubscriber
 */
class InjectUserSubscriberTest extends TestCase
{
    private InjectUserSubscriber $subscriber;
    private MockObject|Security $security;

    protected function setUp(): void
    {
        $this->security   = $this->createMock(Security::class);
        $this->subscriber = new InjectUserSubscriber($this->security);
    }

    /**
     * Test method supports()
     */
    public function testSupports(): void
    {
        $actual = $this->subscriber->supports(new Avatar(), 'POST');

        $this->assertTrue($actual);
    }

    /**
     * Test inject user in Avatar
     */
    public function testInjectUser(): void
    {
        $avatar      = new Avatar();
        $currentUser = new User();

        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($currentUser);

        // Register subscriber
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber($this->subscriber);

        // Build event
        $kernel  = $this->createMock(Kernel::class);
        $request = new Request(attributes: ['data' => $avatar]);
        $request->setMethod('POST');
        $event   = new RequestEvent($kernel, $request, 1);

        // Dispatch event
        $dispatcher->dispatch($event, 'kernel.request');

        $this->assertEquals($currentUser, $avatar->getUser());
    }
}
