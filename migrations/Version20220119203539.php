<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add cascade removal of confirm_email_token & reset_password_request
 */
final class Version20220119203539 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add cascade removal of confirm_email_token & reset_password_request';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE confirm_email_token DROP CONSTRAINT FK_CAF7F958A76ED395');
        $this->addSql('ALTER TABLE confirm_email_token ADD CONSTRAINT FK_CAF7F958A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reset_password_request DROP CONSTRAINT FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE reset_password_request DROP CONSTRAINT fk_7ce748aa76ed395');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT fk_7ce748aa76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE confirm_email_token DROP CONSTRAINT fk_caf7f958a76ed395');
        $this->addSql('ALTER TABLE confirm_email_token ADD CONSTRAINT fk_caf7f958a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
