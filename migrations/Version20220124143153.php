<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220124143153 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE avatar (id UUID NOT NULL, user_id UUID NOT NULL, background_color VARCHAR(9) NOT NULL, hat_color VARCHAR(9) NOT NULL, face_color VARCHAR(9) NOT NULL, hair_color VARCHAR(9) NOT NULL, shirt_color VARCHAR(9) NOT NULL, ear_size VARCHAR(255) NOT NULL, eye_type VARCHAR(255) NOT NULL, hat_type VARCHAR(255) NOT NULL, hair_type VARCHAR(255) NOT NULL, nose_type VARCHAR(255) NOT NULL, mouth_type VARCHAR(255) NOT NULL, shirt_type VARCHAR(255) NOT NULL, glasses_type VARCHAR(255) NOT NULL, shape VARCHAR(255) NOT NULL, earring VARCHAR(255) NOT NULL, eyebrow_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1677722FA76ED395 ON avatar (user_id)');
        $this->addSql('COMMENT ON COLUMN avatar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN avatar.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE avatar ADD CONSTRAINT FK_1677722FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE avatar');
    }
}
