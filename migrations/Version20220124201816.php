<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Remove useless attribute of Avatar
 */
final class Version20220124201816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE avatar DROP background_color');
        $this->addSql('ALTER TABLE avatar DROP shape');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE avatar ADD background_color VARCHAR(9) NOT NULL');
        $this->addSql('ALTER TABLE avatar ADD shape VARCHAR(255) NOT NULL');
    }
}
